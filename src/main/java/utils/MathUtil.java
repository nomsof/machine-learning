package utils;

import java.util.Random;

public class MathUtil {

	/**
	 * Calculate euclidean distance for n-dimension.
	 * 
	 * @param vector1
	 * @param vector2
	 * @return the euclidean distance
	 */
	public static final double calculateDistanse(double[] vector1, double[] vector2) {
		double sum = 0;
		for (int i = 0; i < vector1.length; i++) {
			final double dp = vector1[i] - vector2[i];
			sum += dp * dp;
		}
		return Math.sqrt(sum);
	}

	/**
	 * Generate random integer in range [min,max].
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static final int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	/**
	 * Divide all elements of vector with a specific value.
	 * 
	 * @param vector
	 * @param value
	 * @return
	 */
	public static final double[] devideVector(double[] vector, int value) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = vector[i] / value;
		}
		return vector;
	}
}

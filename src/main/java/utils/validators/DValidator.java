package utils.validators;

import utils.Constants;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class DValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		if (!Constants.IRIS_DATASET.equals(value.toLowerCase()) && !Constants.YEAST_DATASET.equals(value.toLowerCase())) {
			throw new ParameterException("Parameter " + name + " must be " + Constants.IRIS_DATASET + " or "
					+ Constants.YEAST_DATASET);
		}
	}

}

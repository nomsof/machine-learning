package utils;

import java.util.HashMap;
import java.util.Map;

import domain.Cluster;
import domain.DataPoint;

public class PrintStatistics {

	/**
	 * Prints the success & failure rate of the algorithm and the success rate
	 * of each cluster separately.
	 * 
	 * @param clusters
	 *            : The final clusters as a result of algorithm execution
	 * 
	 * */
	public static final void calculatePercentages(Map<Integer, Cluster> clusters) {
		// Find the className that appears most times in each cluster
		Map<Integer, Map<String, Integer>> clusterNames = new HashMap<Integer, Map<String, Integer>>();
		int success = 0;
		int total = 0;
		System.out.println("\n--------------CLUSTERS--------------");
		for (Integer clusterId : clusters.keySet()) {
			Map<String, Integer> classNames = new HashMap<String, Integer>();
			for (DataPoint point : clusters.get(clusterId).getDataPoints()) {
				if (classNames.containsKey(point.getClassName())) {
					classNames.put(point.getClassName(), classNames.get(point.getClassName()) + 1);
				} else {
					classNames.put(point.getClassName(), 1);
				}
			}
			clusterNames.put(clusterId, classNames);
			String clusterName = "";
			for (String name : classNames.keySet()) {
				if (clusterName.isEmpty()) {
					clusterName = name;
				} else if (classNames.get(name) > classNames.get(clusterName)) {
					clusterName = name;
				}
			}
			success += classNames.get(clusterName);
			total += clusters.get(clusterId).getDataPoints().size();

			// The success percentage of each cluster
			double percentage = (classNames.get(clusterName) * 100.0f) / clusters.get(clusterId).getDataPoints().size();
			System.out.println("Cluster " + clusterName + " : " + percentage + "%");

		}
		System.out.println("-------------------------------------\n");
		// Find the success percentage for all clusters
		System.out.println("------------------------------------------");
		double totalPercentage = (success * 100.0f) / total;
		System.out.println("Total percentage: " + totalPercentage + "%");
		double errorPercentage = (total - success) * 100.0f / total;
		System.out.println("Error percentage: " + errorPercentage + "%");
		System.out.println("------------------------------------------");
	}
}

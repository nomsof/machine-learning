package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import domain.DataPoint;
import domain.DataSet;

public class CSVReader {

	private static CSVParser parser;
	private static List<DataPoint> dataPoints = new ArrayList<DataPoint>();

	public static List<DataPoint> parseData() {
		try {
			// Initialize CSVParser object
			parser = new CSVParser(new BufferedReader(new InputStreamReader(CSVReader.class.getClassLoader()
					.getResourceAsStream(DataSet.CSV))), CSVFormat.DEFAULT.withHeader());
			List<CSVRecord> csvRecords = parser.getRecords();
			String[] headerValues = DataSet.HEADER.split(",");
			double[] vector = new double[DataSet.DIMENSION];
			for (CSVRecord record : csvRecords) {
				try {
					for (int i = 0; i < DataSet.DIMENSION; i++) {
						vector[i] = Double.parseDouble(record.get(headerValues[i]));
					}
					dataPoints.add(new DataPoint(vector, record.get(headerValues[headerValues.length - 1])));
				} catch (NumberFormatException e) {
					System.out.println("Error while converting String to double. Number: " + e.getMessage());
				}
			}
		} catch (IOException e) {
			System.out.println("Error while reading csv file: " + e);
		}
		return dataPoints;
	}
}
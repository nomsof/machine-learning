package utils;

public class Constants {

	public static final String IRIS_DATASET = "iris";
	public static final String IRIS_PATH_TO_CSV = "iris.csv";
	public static final String IRIS_HEADERS = "sepalLength,sepalWidth,petalLength,petalWidth,class";
	public static final int IRIS_DIMENSION = 4;
	public static final int IRIS_K = 3;

	public static final String YEAST_DATASET = "yeast";
	public static final String YEAST_PATH_TO_CSV = "yeast.csv";
	public static final String YEAST_HEADERS = "mcg,gvh,alm,mit,erl,pox,vac,nuc,class";
	public static final int YEAST_DIMENSION = 8;
	public static final int YEAST_K = 10;
}

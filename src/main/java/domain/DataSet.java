package domain;

public class DataSet {

	public static String HEADER;
	public static int DIMENSION;
	public static String CSV;
	public static int K;

	public static final void initialize(String header, int dimension, String csv, int k) {
		HEADER = header;
		DIMENSION = dimension;
		CSV = csv;
		K = k;
	}
}

package domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cluster {

	private double[] centroidVector;
	private List<DataPoint> dataPoints = new ArrayList<DataPoint>();

	public Cluster() {
	}

	public Cluster(double[] centroidVector) {
		this.centroidVector = centroidVector.clone();
	}

	public double[] getCentroidVector() {
		return centroidVector;
	}

	public void setCentroidVector(double[] centroidVector) {
		this.centroidVector = centroidVector.clone();
	}

	public List<DataPoint> getDataPoints() {
		return dataPoints;
	}

	public void assignDataPoint(DataPoint point) {
		dataPoints.add(point);
	}

	@Override
	public String toString() {
		return "\nCluster [centroidVector=" + Arrays.toString(centroidVector) + ", dataPoints=" + dataPoints + "]";
	}
}

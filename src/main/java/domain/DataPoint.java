package domain;

import java.util.Arrays;

public class DataPoint {
	
	private double[] vector;
	private String className;
	
	public DataPoint(){}
	
	public DataPoint(double[] vector, String className){
		this.vector = vector.clone();
		this.className = className;
	}

	public double[] getVector() {
		return vector;
	}

	public void setVector(double[] vector) {
		this.vector = vector.clone();
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return "\nDataPoint [vector=" + Arrays.toString(vector) + ", className=" + className + "]";
	}
}

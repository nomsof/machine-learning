package algorithm;

import utils.CSVReader;
import utils.Constants;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import domain.DataSet;

public class KMeansRunner {

	public static void main(String[] args) {

		// Load algorithm parameters
		KMeansParams params = new KMeansParams();
		JCommander cmd = new JCommander(params);
		try {
			cmd.parse(args);
		} catch (ParameterException e) {
			System.out.println(e.getMessage());
			cmd.usage();
			return;
		}

		// Initialize DataSet
		if (Constants.IRIS_DATASET.equals(params.getDataSet())) {
			DataSet.initialize(Constants.IRIS_HEADERS, Constants.IRIS_DIMENSION, Constants.IRIS_PATH_TO_CSV,
					Constants.IRIS_K);
		} else {
			DataSet.initialize(Constants.YEAST_HEADERS, Constants.YEAST_DIMENSION, Constants.YEAST_PATH_TO_CSV,
					Constants.YEAST_K);
		}

		// Call kmeans algorithm
		KMeans.execute(DataSet.K, CSVReader.parseData());
	}
}

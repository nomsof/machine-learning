package algorithm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import utils.MathUtil;
import utils.PrintStatistics;
import domain.Cluster;
import domain.DataPoint;
import domain.DataSet;

public class KMeans {

	private static boolean sameCentroids = false;
	private static Map<Integer, Cluster> clusters = new HashMap<Integer, Cluster>();

	public static final void execute(int k, List<DataPoint> dataPoints) {

		// Assign random centroids
		int step = dataPoints.size() / k;
		for (int i = 0; i < k; i++) {
			clusters.put(i, new Cluster(dataPoints.get(MathUtil.randInt(step * i, step * (i + 1) - 1)).getVector()));
		}

		while (!sameCentroids) {
			// Clear data points
			for (Cluster cluster : clusters.values()) {
				cluster.getDataPoints().clear();
			}

			// Assign points to clusters
			for (DataPoint point : dataPoints) {
				int assignTo = 0;
				double minDistance = MathUtil.calculateDistanse(clusters.get(0).getCentroidVector(), point.getVector());
				for (int i = 1; i < k; i++) {
					double distance = MathUtil
							.calculateDistanse(clusters.get(i).getCentroidVector(), point.getVector());
					if (Double.compare(distance, minDistance) < 0) {
						minDistance = distance;
						assignTo = i;
					}
				}
				clusters.get(assignTo).assignDataPoint(point);
			}

			// Recalculate cluster centroids
			double[] newCentroidVector = new double[DataSet.DIMENSION];
			sameCentroids = true;
			for (int i = 0; i < k; i++) {
				Cluster cluster = clusters.get(i);
				List<DataPoint> points = cluster.getDataPoints();
				Iterator<DataPoint> pointIt = points.iterator();

				// Find new centroid
				newCentroidVector = pointIt.next().getVector().clone();
				while (pointIt.hasNext()) {
					DataPoint point = pointIt.next();
					for (int j = 0; j < DataSet.DIMENSION; j++) {
						newCentroidVector[j] += point.getVector()[j];
					}
				}
				newCentroidVector = MathUtil.devideVector(newCentroidVector, points.size());

				// Assign new centroid to cluster
				if (!Arrays.equals(newCentroidVector, cluster.getCentroidVector())) {
					cluster.setCentroidVector(newCentroidVector);
					sameCentroids = false;
				}
			}
		}
		PrintStatistics.calculatePercentages(clusters);
	}
}

package algorithm;

import utils.validators.DValidator;

import com.beust.jcommander.Parameter;

public class KMeansParams {
	@Parameter(names = "-dataSet", description = "Input Data Set. Default: iris", required = false, validateWith = DValidator.class)
	private String dataSet = "iris";

	public String getDataSet() {
		return dataSet.toLowerCase();
	}

}
